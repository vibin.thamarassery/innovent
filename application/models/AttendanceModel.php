<?php 
class AttendanceModel extends CI_Model{
   
    public function getStudentsAttendance($class_id='',$keyword='') 
	{
		$field = 'S.student_roll_no,S.student_name';
    	$this->db->select('S.id, S.student_roll_no, S.student_name, A.attendance_status, A.attendance_date');
    	$this->db->from('inno_students S');
    	$this->db->join('inno_student_attendance A', 'A.student_id = S.id and A.attendance_date = CURDATE()', 'left'); 
		if($class_id != '')
		{
			$this->db->where('S.class_id',$class_id);
		}
		if($keyword != '')
		{
			$this->db->like('concat('.$field.')',$keyword);
		}
    	$query = $this->db->get();
    	if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAttendanceStatus($student_id='',$from='',$to='') 
	{
		if($student_id =='')
		{
			return false;
		}
        $this->db->select('attendance_status, attendance_date');
        $this->db->where('student_id', $student_id);
        if($from == '')
		{
			$this->db->where('attendance_date', 'CURDATE()', FALSE);
		}
		else
		{
			$this->db->where('attendance_date >=', $from);
			$this->db->where('attendance_date <=', $to);
		}
        $query = $this->db->get('inno_student_attendance');
        if ($query->num_rows() > 0) 
		{
            return $query->result();
        } 
		else 
		{
            return false;
        }   
    }
	
    public function mark($data=array())
    {
        return $this->db->insert('inno_student_attendance', $data);
    }

    public function delete($student_id='') 
    {
        $this->db->where('student_id', $student_id);
		$this->db->where('attendance_date', 'CURDATE()', FALSE);
        $this->db->delete('inno_student_attendance');
        return true;
    }

}