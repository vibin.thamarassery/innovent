<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');

class Attendance extends REST_Controller
{  
    function __construct()
    { 
        parent::__construct();
		$this->load->database();
        $this->load->model('attendanceModel','attendance');
    }
	/* function to get report of all students with id,name,roll number, attendance and date */
	public function index_get($class_id='',$keyword='')
    { 
        $students = $this->attendance->getStudentsAttendance($class_id,$keyword);
		if($students)
        {
            $status = 1;
        }
        else
        {
			$status = 0;
        }
        $this->response(array('status' => $status,'students'=>$students));
    }
    /* If there is no data in the attendance table means the student is present */	
	/* Function to mark absents of the student, else delete the row. */
	public function markAttendnace_post() 
	{
		/* 1 for absent, 2 for present */
        $studentId 	= $this->post('student_id');
        $currentStatus = $this->post('status');
        if($currentStatus == 2) 
		{
			/*Delete the row if the student is present */
            $present 	= $this->attendance->delete($studentId); 
			if($present)
			{
				$message = "Successfully marked as present";
				$status  = 1; 
			}
			else
			{
				$message = "There is an issue to mark present!";
				$status  = 0; 
			}                       
        } 
		else 
		{
            $data = array(
                'attendance_status' => $currentStatus,
                'student_id' 		=> $studentId,
                'attendance_date' 	=> date('Y-m-d')
            );
			/* Mark absent */
            if($this->attendance->mark($data))
			{
                $message = "Successfully marked as absent";
				$status  = 1;
            }
			else
			{
				$message = "There is an issue to mark absent!";
				$status  = 0;
			}
        }
        $this->response(array('status' => $status,'message'=>$message));
    }
    /* Function to get attendnace status of a student by id and date range */
    public function getAttendanceStatus_get($student_id='',$from='',$to='') 
	{
        $attendance = $this->attendance->getAttendanceStatus($student_id,$from,$to);
        if($attendance)
        {
            $status = 1; /* Absent */
        }
        else
        {
			$status = 0; /* Present */
        }
        $this->response(array('status' => $status,'attendance'=>$attendance));
    }   
}