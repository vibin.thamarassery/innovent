-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2019 at 09:23 AM
-- Server version: 5.6.17
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innovation`
--

-- --------------------------------------------------------

--
-- Table structure for table `inno_students`
--

CREATE TABLE IF NOT EXISTS `inno_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_roll_no` varchar(10) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `school_id` int(11) NOT NULL DEFAULT '1',
  `class_id` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `inno_students`
--

INSERT INTO `inno_students` (`id`, `student_roll_no`, `student_name`, `school_id`, `class_id`, `created_by`, `created_date`, `updated_date`) VALUES
(1, '1001', 'Muhammad Ali', 1, 1, 1, '2019-07-07 21:30:27', '2019-07-07 21:30:27'),
(2, '1002', 'Omar Almejeri', 1, 1, 1, '2019-07-07 21:30:27', '2019-07-07 21:30:27'),
(3, '1003', 'Mubeena Thahir', 1, 1, 1, '2019-07-07 21:30:27', '2019-07-07 21:30:27'),
(4, '1004', 'Muhammad Azam', 1, 2, 1, '2019-07-07 21:30:27', '2019-07-07 21:30:27'),
(5, '1005', 'Tariq Hussain', 1, 2, 1, '2019-07-07 21:30:27', '2019-07-07 21:30:27');

-- --------------------------------------------------------

--
-- Table structure for table `inno_student_attendance`
--

CREATE TABLE IF NOT EXISTS `inno_student_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `attendance_status` int(1) NOT NULL,
  `attendance_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `inno_student_attendance`
--

INSERT INTO `inno_student_attendance` (`id`, `student_id`, `attendance_status`, `attendance_date`) VALUES
(2, 1, 1, '2019-07-08'),
(7, 2, 1, '2019-07-09'),
(8, 1, 1, '2019-07-09');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inno_student_attendance`
--
ALTER TABLE `inno_student_attendance`
  ADD CONSTRAINT `inno_student_attendance_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `inno_students` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
